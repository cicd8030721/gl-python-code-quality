# https://github.com/soul-catcher/mypy-gitlab-code-quality
import hashlib
import json
import re
from sys import stdin
from typing import Any, TextIO

SEVERITY = {
    "note": "info",
    "error": "major",
}


def get_hash(tpl: tuple[str | Any, ...]) -> str:
    return hashlib.md5(json.dumps(tpl).encode("utf-8")).hexdigest()


def is_info_to_previous_issue(
        issues: list[dict], severity: str, line_number: int
) -> bool:
    return ((len(issues) != 0)
            and (severity == "info")
            and (issues[-1]["location"]["lines"]["begin"] == line_number)
            )


def append_line_to_issues(
        issues: list[dict],
        fingerprint: str,
        severity: str,
        line_number: int,
        description: str,
        path: str,
) -> None:
    if is_info_to_previous_issue(issues, severity, line_number):
        issues[-1]["description"] += f"\n{description}"
    else:
        issues.append(
            {
                "description": description,
                "fingerprint": fingerprint,
                "severity": severity,
                "location": {
                    "path": path,
                    "lines": {
                        "begin": line_number,
                    },
                },
            }
        )


def parse_lines(lines: TextIO) -> list[dict]:
    issues: list[dict] = []
    for input_line in lines:
        line = input_line.rstrip("\n")
        match = re.fullmatch(
            r"(?P<path>.+?)"
            r":(?P<line>\d+)"
            r":(?:\d+:)?\s(?P<error_level>\w+)"
            r":\s(?P<description>.+)",
            line,
        )
        if match is None:
            continue
        fingerprint: str = get_hash(match.groups())
        severity: str = SEVERITY.get(match["error_level"], "unknown")
        line_number: int = int(match["line"])
        description: str = match["description"]
        path: str = match["path"]
        append_line_to_issues(
            issues, fingerprint, severity, line_number, description, path
        )
    return issues


if __name__ == '__main__':
    print(
        json.dumps(
            parse_lines(stdin),
            indent="\t",
        )
    )
