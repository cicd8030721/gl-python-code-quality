import os
import sys
import tomllib
from pathlib import Path

import tomli_w

if os.getenv("RUFF_FORCE", False) == "true":
    sys.exit(0)


def has_tool_ruff(x: dict) -> bool:
    return x.get("tool", {}).get("ruff") is not None


for file in filter(lambda x: has_tool_ruff(tomllib.load(x.open("rb"))), list(Path(Path.cwd()).glob("*.toml"))):
    file_config = tomllib.load(file.open("rb"))
    file_config["tool"]["ruff"]["extend"] = "/pyproject.toml"
    tomli_w.dump(file_config, file.open("wb"))
