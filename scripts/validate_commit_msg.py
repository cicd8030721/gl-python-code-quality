import sys
from sys import stdin

print("normal output", file=sys.stdout)
print("error output", file=sys.stderr)

SEMANTIC_REGEX = r"^(fix|feat|docs|doc|breaking|revert|chore|perf|test|style|refactor)(\([\w\s]+\))?!?.*"
commit_msg = stdin.readline()
